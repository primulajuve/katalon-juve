<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Kolom bertanda (), WAJIB DIISI</name>
   <tag></tag>
   <elementGuidId>07a6f258-d0ec-4660-aba6-0920876a8812</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/following::p[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>3644497a-32f8-4e2a-beee-13cd285eac7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Kolom bertanda (*), WAJIB DIISI.</value>
      <webElementGuid>dae4471a-b507-48fe-ac17-2eaa6c45fb21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;page-container&quot;]/div[@class=&quot;page-content&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;portlet light&quot;]/div[@class=&quot;portlet-body&quot;]/div[@class=&quot;note note-danger&quot;]/p[1]</value>
      <webElementGuid>94c3dce4-32f9-40f7-a337-6246c27f65a5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/following::p[1]</value>
      <webElementGuid>cf601bd4-b143-4475-8b32-f95fa3aee348</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Registrasi'])[2]/following::p[1]</value>
      <webElementGuid>b33f4bb4-6e30-4f74-b62f-5d3ec323aa72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi Pengguna'])[1]/preceding::p[1]</value>
      <webElementGuid>c7bef339-038f-4da8-8680-19a89c4d6b4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kolom bertanda (*), WAJIB DIISI.']/parent::*</value>
      <webElementGuid>ca0b0d66-c82e-42bc-af3e-b62999dcecbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>9ec8bc9e-53f5-4ebe-9a54-36bfac08b7c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Kolom bertanda (*), WAJIB DIISI.' or . = 'Kolom bertanda (*), WAJIB DIISI.')]</value>
      <webElementGuid>2496d1dd-44d1-43d1-97ca-1f1df99bf22c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
