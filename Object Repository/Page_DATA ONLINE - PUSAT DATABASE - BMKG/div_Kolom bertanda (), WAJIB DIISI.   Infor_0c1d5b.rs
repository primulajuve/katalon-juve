<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Kolom bertanda (), WAJIB DIISI.   Infor_0c1d5b</name>
   <tag></tag>
   <elementGuidId>566e69c7-3cf0-475c-9d47-47879bb7ca13</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.portlet-body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3be425a7-8b5f-405d-b418-2d613dd1be7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>portlet-body</value>
      <webElementGuid>0a53c0eb-9a64-4286-a668-43d772a5ef1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		
								Kolom bertanda (*), WAJIB DIISI.
		
			
	 
		


		
			
			
				
					  Informasi Pengguna
				
			
			
		
		
			
				
					Email  *  
					
					Pastikan email anda aktif, email anda akan digunakan sebagai nama pengguna untuk masuk ke aplikasi
					 
				

				
					Sandi  *  
					
					(Minimal panjang 5 Karakter)
				

				
					Konfirmasi Sandi  *  
					
					(Minimal panjang 5 Karakter)
				
				
					Nama Lengkap  *  
					
					       
                    
						
				
				
				
					Negara  *  
					
						 --Pilih--
																					Afghanistan
															Albania
															Algeria
															Angola
															Antigua and Barbuda
															Argentina
															Armenia
															Australia
															Austria
															Azerbaijan
															Bahamas
															Bangladesh
															Barbados
															Belarus
															Belgium
															Belize
															Benin
															Bhutan
															Bolivia
															Bosnia and Herzegovina
															Botswana
															Brazil
															Brunei Darussalam
															Bulgaria
															Burkina Faso
															Burundi
															Cambodia
															Cameroon
															Canada
															Cape Verde
															Central African Republic
															Chad
															Chile
															China
															Colombia
															Comoros
															Congo
															Congo
															Costa Rica
															Cote d'Ivoire
															Croatia
															Cuba
															Cyprus
															Czech Republic
															Denmark
															Djibouti
															Dominica
															Dominican Republic
															Ecuador
															Egypt
															El Salvador
															Equatorial Guinea
															Eritrea
															Estonia
															Ethiopia
															Falkland Islands
															Fiji
															Finland
															France
															French Guiana
															French Polynesia
															Gabon
															Gambia
															Georgia
															Germany
															Ghana
															Greece
															Greenland
															Grenada
															Guatemala
															Guinea
															Guinea-Bissau
															Guyana
															Haiti
															Honduras
															Hungary
															Iceland
															India
															Indonesia
															Iran
															Iraq
															Ireland
															Israel
															Italy
															Jamaica
															Japan
															Jordan
															Kazakhstan
															Kenya
															Kuwait
															Kyrgyz Republic
															Lao People's Democratic Republic
															Latvia
															Lebanon
															Lesotho
															Liberia
															Libya
															Lithuania
															Macedonia
															Madagascar
															Malawi
															Malaysia
															Maldives
															Mali
															Malta
															Mauritania
															Mauritius
															Mexico
															Moldova
															Mongolia
															Morocco
															Mozambique
															Myanmar
															Namibia
															Nepal
															Netherlands
															New Caledonia
															New Zealand
															Nicaragua
															Niger
															Nigeria
															North Korea
															Norway
															Oman
															Pakistan
															Panama
															Papua New Guinea
															Paraguay
															Peru
															Philippines
															Poland
															Portugal
															Qatar
															Reunion
															Romania
															Russian Federation
															Rwanda
															Saint Kitts and Nevis
															Saint Lucia
															Sao Tome and Principe
															Saudi Arabia
															Senegal
															Serbia
															Seychelles
															Sierra Leone
															Slovakia
															Slovenia
															Solomon Islands
															Somalia
															South Africa
															South Korea
															Spain
															Sri Lanka
															Sudan
															Suriname
															Swaziland
															Sweden
															Switzerland
															Syrian Arab Republic
															Taiwan
															Tajikistan
															Tanzania
															Thailand
															Timor-Leste
															Togo
															Trinidad and Tobago
															Tunisia
															Turkey
															Turkmenistan
															Uganda
															Ukraine
															United Arab Emirates
															United Kingdom
															United States of America
															Uruguay
															Uzbekistan
															Vanuatu
															Venezuela
															Vietnam
															Yemen
															Zambia
															Zimbabwe
											    						 --Pilih--
				
				
				
				
					Kategori Pekerjaan  *  
					
						 --Pilih--
																					
																			BUMN																	
															
																			Dosen																	
															
																			Instansi pemerintah																	
															
																			Mahasiswa																	
															
																			Peneliti																	
															
																			Swasta																	
											    						
				

				
					Instansi  *  
					
					
						 --Pilih--
					
					
					
					
					
					
					
				
				
					Tujuan Penggunaan Data  *  
					
						 --Pilih--
																					
																			Kegiatan operasional dan pengembangan produk																	
															
																			Layanan meteorologi																	
															
																			Penelitian tentang data meteorologi																	
											    						
				

				
					
						Untuk mendukung manajemen pengguna layanan Data Online BMKG dalam rangka meningkatkan layanan, pendaftar harus memberikan informasi data diri secara benar dan memperbarui data diri ketika terdapat perubahan.

Informasi pendaftar bersifat rahasia sehingga harus dilindungi dan hanya boleh dimanfaatkan untuk peningkatan layanan Data Online.

Khusus untuk tujuan penelitian, pengguna harus menyertakan referensi data yang didapatkan dari Data Online.					
					     
					Saya telah membaca dan menyetujui ketentuan Pendaftaran Akun Data Online
				
				
				
					Masukkan kode berikut *  

					
						
													
						
					

					
						
					
				

				
			
		

	</value>
      <webElementGuid>ff7e1896-bf36-43b6-903d-951e1467db71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;page-container&quot;]/div[@class=&quot;page-content&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;portlet light&quot;]/div[@class=&quot;portlet-body&quot;]</value>
      <webElementGuid>381f4dd9-998b-484f-9aaf-33b8f6dac557</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/following::div[3]</value>
      <webElementGuid>efe8f295-38a8-4a5c-9945-7c06dda0f1dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Registrasi'])[2]/following::div[6]</value>
      <webElementGuid>3f47cf5c-0cb4-4300-81ea-89cb967192c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div</value>
      <webElementGuid>e3096f34-db2d-4bd0-a8e4-64298db2653b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
		
								Kolom bertanda (*), WAJIB DIISI.
		
			
	 
		


		
			
			
				
					  Informasi Pengguna
				
			
			
		
		
			
				
					Email  *  
					
					Pastikan email anda aktif, email anda akan digunakan sebagai nama pengguna untuk masuk ke aplikasi
					 
				

				
					Sandi  *  
					
					(Minimal panjang 5 Karakter)
				

				
					Konfirmasi Sandi  *  
					
					(Minimal panjang 5 Karakter)
				
				
					Nama Lengkap  *  
					
					       
                    
						
				
				
				
					Negara  *  
					
						 --Pilih--
																					Afghanistan
															Albania
															Algeria
															Angola
															Antigua and Barbuda
															Argentina
															Armenia
															Australia
															Austria
															Azerbaijan
															Bahamas
															Bangladesh
															Barbados
															Belarus
															Belgium
															Belize
															Benin
															Bhutan
															Bolivia
															Bosnia and Herzegovina
															Botswana
															Brazil
															Brunei Darussalam
															Bulgaria
															Burkina Faso
															Burundi
															Cambodia
															Cameroon
															Canada
															Cape Verde
															Central African Republic
															Chad
															Chile
															China
															Colombia
															Comoros
															Congo
															Congo
															Costa Rica
															Cote d&quot; , &quot;'&quot; , &quot;Ivoire
															Croatia
															Cuba
															Cyprus
															Czech Republic
															Denmark
															Djibouti
															Dominica
															Dominican Republic
															Ecuador
															Egypt
															El Salvador
															Equatorial Guinea
															Eritrea
															Estonia
															Ethiopia
															Falkland Islands
															Fiji
															Finland
															France
															French Guiana
															French Polynesia
															Gabon
															Gambia
															Georgia
															Germany
															Ghana
															Greece
															Greenland
															Grenada
															Guatemala
															Guinea
															Guinea-Bissau
															Guyana
															Haiti
															Honduras
															Hungary
															Iceland
															India
															Indonesia
															Iran
															Iraq
															Ireland
															Israel
															Italy
															Jamaica
															Japan
															Jordan
															Kazakhstan
															Kenya
															Kuwait
															Kyrgyz Republic
															Lao People&quot; , &quot;'&quot; , &quot;s Democratic Republic
															Latvia
															Lebanon
															Lesotho
															Liberia
															Libya
															Lithuania
															Macedonia
															Madagascar
															Malawi
															Malaysia
															Maldives
															Mali
															Malta
															Mauritania
															Mauritius
															Mexico
															Moldova
															Mongolia
															Morocco
															Mozambique
															Myanmar
															Namibia
															Nepal
															Netherlands
															New Caledonia
															New Zealand
															Nicaragua
															Niger
															Nigeria
															North Korea
															Norway
															Oman
															Pakistan
															Panama
															Papua New Guinea
															Paraguay
															Peru
															Philippines
															Poland
															Portugal
															Qatar
															Reunion
															Romania
															Russian Federation
															Rwanda
															Saint Kitts and Nevis
															Saint Lucia
															Sao Tome and Principe
															Saudi Arabia
															Senegal
															Serbia
															Seychelles
															Sierra Leone
															Slovakia
															Slovenia
															Solomon Islands
															Somalia
															South Africa
															South Korea
															Spain
															Sri Lanka
															Sudan
															Suriname
															Swaziland
															Sweden
															Switzerland
															Syrian Arab Republic
															Taiwan
															Tajikistan
															Tanzania
															Thailand
															Timor-Leste
															Togo
															Trinidad and Tobago
															Tunisia
															Turkey
															Turkmenistan
															Uganda
															Ukraine
															United Arab Emirates
															United Kingdom
															United States of America
															Uruguay
															Uzbekistan
															Vanuatu
															Venezuela
															Vietnam
															Yemen
															Zambia
															Zimbabwe
											    						 --Pilih--
				
				
				
				
					Kategori Pekerjaan  *  
					
						 --Pilih--
																					
																			BUMN																	
															
																			Dosen																	
															
																			Instansi pemerintah																	
															
																			Mahasiswa																	
															
																			Peneliti																	
															
																			Swasta																	
											    						
				

				
					Instansi  *  
					
					
						 --Pilih--
					
					
					
					
					
					
					
				
				
					Tujuan Penggunaan Data  *  
					
						 --Pilih--
																					
																			Kegiatan operasional dan pengembangan produk																	
															
																			Layanan meteorologi																	
															
																			Penelitian tentang data meteorologi																	
											    						
				

				
					
						Untuk mendukung manajemen pengguna layanan Data Online BMKG dalam rangka meningkatkan layanan, pendaftar harus memberikan informasi data diri secara benar dan memperbarui data diri ketika terdapat perubahan.

Informasi pendaftar bersifat rahasia sehingga harus dilindungi dan hanya boleh dimanfaatkan untuk peningkatan layanan Data Online.

Khusus untuk tujuan penelitian, pengguna harus menyertakan referensi data yang didapatkan dari Data Online.					
					     
					Saya telah membaca dan menyetujui ketentuan Pendaftaran Akun Data Online
				
				
				
					Masukkan kode berikut *  

					
						
													
						
					

					
						
					
				

				
			
		

	&quot;) or . = concat(&quot;
		
								Kolom bertanda (*), WAJIB DIISI.
		
			
	 
		


		
			
			
				
					  Informasi Pengguna
				
			
			
		
		
			
				
					Email  *  
					
					Pastikan email anda aktif, email anda akan digunakan sebagai nama pengguna untuk masuk ke aplikasi
					 
				

				
					Sandi  *  
					
					(Minimal panjang 5 Karakter)
				

				
					Konfirmasi Sandi  *  
					
					(Minimal panjang 5 Karakter)
				
				
					Nama Lengkap  *  
					
					       
                    
						
				
				
				
					Negara  *  
					
						 --Pilih--
																					Afghanistan
															Albania
															Algeria
															Angola
															Antigua and Barbuda
															Argentina
															Armenia
															Australia
															Austria
															Azerbaijan
															Bahamas
															Bangladesh
															Barbados
															Belarus
															Belgium
															Belize
															Benin
															Bhutan
															Bolivia
															Bosnia and Herzegovina
															Botswana
															Brazil
															Brunei Darussalam
															Bulgaria
															Burkina Faso
															Burundi
															Cambodia
															Cameroon
															Canada
															Cape Verde
															Central African Republic
															Chad
															Chile
															China
															Colombia
															Comoros
															Congo
															Congo
															Costa Rica
															Cote d&quot; , &quot;'&quot; , &quot;Ivoire
															Croatia
															Cuba
															Cyprus
															Czech Republic
															Denmark
															Djibouti
															Dominica
															Dominican Republic
															Ecuador
															Egypt
															El Salvador
															Equatorial Guinea
															Eritrea
															Estonia
															Ethiopia
															Falkland Islands
															Fiji
															Finland
															France
															French Guiana
															French Polynesia
															Gabon
															Gambia
															Georgia
															Germany
															Ghana
															Greece
															Greenland
															Grenada
															Guatemala
															Guinea
															Guinea-Bissau
															Guyana
															Haiti
															Honduras
															Hungary
															Iceland
															India
															Indonesia
															Iran
															Iraq
															Ireland
															Israel
															Italy
															Jamaica
															Japan
															Jordan
															Kazakhstan
															Kenya
															Kuwait
															Kyrgyz Republic
															Lao People&quot; , &quot;'&quot; , &quot;s Democratic Republic
															Latvia
															Lebanon
															Lesotho
															Liberia
															Libya
															Lithuania
															Macedonia
															Madagascar
															Malawi
															Malaysia
															Maldives
															Mali
															Malta
															Mauritania
															Mauritius
															Mexico
															Moldova
															Mongolia
															Morocco
															Mozambique
															Myanmar
															Namibia
															Nepal
															Netherlands
															New Caledonia
															New Zealand
															Nicaragua
															Niger
															Nigeria
															North Korea
															Norway
															Oman
															Pakistan
															Panama
															Papua New Guinea
															Paraguay
															Peru
															Philippines
															Poland
															Portugal
															Qatar
															Reunion
															Romania
															Russian Federation
															Rwanda
															Saint Kitts and Nevis
															Saint Lucia
															Sao Tome and Principe
															Saudi Arabia
															Senegal
															Serbia
															Seychelles
															Sierra Leone
															Slovakia
															Slovenia
															Solomon Islands
															Somalia
															South Africa
															South Korea
															Spain
															Sri Lanka
															Sudan
															Suriname
															Swaziland
															Sweden
															Switzerland
															Syrian Arab Republic
															Taiwan
															Tajikistan
															Tanzania
															Thailand
															Timor-Leste
															Togo
															Trinidad and Tobago
															Tunisia
															Turkey
															Turkmenistan
															Uganda
															Ukraine
															United Arab Emirates
															United Kingdom
															United States of America
															Uruguay
															Uzbekistan
															Vanuatu
															Venezuela
															Vietnam
															Yemen
															Zambia
															Zimbabwe
											    						 --Pilih--
				
				
				
				
					Kategori Pekerjaan  *  
					
						 --Pilih--
																					
																			BUMN																	
															
																			Dosen																	
															
																			Instansi pemerintah																	
															
																			Mahasiswa																	
															
																			Peneliti																	
															
																			Swasta																	
											    						
				

				
					Instansi  *  
					
					
						 --Pilih--
					
					
					
					
					
					
					
				
				
					Tujuan Penggunaan Data  *  
					
						 --Pilih--
																					
																			Kegiatan operasional dan pengembangan produk																	
															
																			Layanan meteorologi																	
															
																			Penelitian tentang data meteorologi																	
											    						
				

				
					
						Untuk mendukung manajemen pengguna layanan Data Online BMKG dalam rangka meningkatkan layanan, pendaftar harus memberikan informasi data diri secara benar dan memperbarui data diri ketika terdapat perubahan.

Informasi pendaftar bersifat rahasia sehingga harus dilindungi dan hanya boleh dimanfaatkan untuk peningkatan layanan Data Online.

Khusus untuk tujuan penelitian, pengguna harus menyertakan referensi data yang didapatkan dari Data Online.					
					     
					Saya telah membaca dan menyetujui ketentuan Pendaftaran Akun Data Online
				
				
				
					Masukkan kode berikut *  

					
						
													
						
					

					
						
					
				

				
			
		

	&quot;))]</value>
      <webElementGuid>e11b53b6-790b-4de0-9332-a90e6d96f255</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
