<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Email    Pastikan email anda aktif, ema_d231e1</name>
   <tag></tag>
   <elementGuidId>b1835b14-0ed2-4fd8-9f57-50bec21b3557</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form-validate-register']/div/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-md-7 > div.form-group</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>06f94322-66a6-480e-affb-3e1d9225dedb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-group</value>
      <webElementGuid>a3c997ec-6a11-4da5-97b2-6823c77328b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					Email  *  
					
					Pastikan email anda aktif, email anda akan digunakan sebagai nama pengguna untuk masuk ke aplikasi
					 
				</value>
      <webElementGuid>d62672ab-8c40-48b5-8e1a-d217c93fcae1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-validate-register&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7&quot;]/div[@class=&quot;form-group&quot;]</value>
      <webElementGuid>aa040d83-1bf9-40a8-b305-a238bae5a65c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate-register']/div/div[2]/div</value>
      <webElementGuid>7a36e9c0-dc19-4432-9800-47ef4f455d6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi Pengguna'])[1]/following::div[2]</value>
      <webElementGuid>759e08ec-16b0-450a-b870-8bb1fa7c61d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/following::div[10]</value>
      <webElementGuid>90340735-2ced-4dab-86d7-6176a4d0d2b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div[2]/div</value>
      <webElementGuid>c2b87c80-c40d-4262-9a82-8779a8919331</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
					Email  *  
					
					Pastikan email anda aktif, email anda akan digunakan sebagai nama pengguna untuk masuk ke aplikasi
					 
				' or . = '
					Email  *  
					
					Pastikan email anda aktif, email anda akan digunakan sebagai nama pengguna untuk masuk ke aplikasi
					 
				')]</value>
      <webElementGuid>26a63406-3458-4b08-97bd-fbef4fa4b6fb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
