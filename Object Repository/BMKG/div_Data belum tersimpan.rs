<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Data belum tersimpan</name>
   <tag></tag>
   <elementGuidId>9423be7e-49af-4f77-9771-38851ae0678f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.notify-message.alert-danger > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>06e93eae-cbef-48e9-bda9-dfa46bad4b1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Data belum tersimpan *</value>
      <webElementGuid>af1093f2-18c3-4294-bb1e-3e43b69b8daf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;notify notify-top-center&quot;]/div[@class=&quot;alert notify-message alert-danger&quot;]/div[1]</value>
      <webElementGuid>a9071632-c806-41c7-bba8-3beeb7ec8277</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[4]</value>
      <webElementGuid>33bc7e3c-f43d-4013-8260-f585b7166d6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::div[4]</value>
      <webElementGuid>a17d452f-c07f-4e69-a45b-a19364ad3058</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Data belum tersimpan *']/parent::*</value>
      <webElementGuid>81ccf970-ebff-4032-bbaf-239396b04065</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div</value>
      <webElementGuid>6fcd050d-3401-451e-bae2-c9c8e00db11a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Data belum tersimpan *' or . = 'Data belum tersimpan *')]</value>
      <webElementGuid>069554ab-60af-41e9-a40d-ddd67049c819</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
