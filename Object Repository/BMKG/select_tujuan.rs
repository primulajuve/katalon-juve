<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_tujuan</name>
   <tag></tag>
   <elementGuidId>235a4923-ee93-4697-a404-c7b27aab759b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;tujuan-penggunaan&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='tujuan-penggunaan']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>65dc8eb1-6ad3-4043-bf25-a4e3ea87489c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>tujuan-penggunaan</value>
      <webElementGuid>ea753d52-139f-4fa3-8757-67ee5fc10d62</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2 form-control</value>
      <webElementGuid>9b14e310-a3b3-446e-afd3-e4cf7f1f55e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						 --Pilih--
																					
																			Kegiatan operasional dan pengembangan produk																	
															
																			Layanan meteorologi																	
															
																			Penelitian tentang data meteorologi																	
											    						</value>
      <webElementGuid>1eb26e6b-0e89-4493-99fc-cfc68fbb6c6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-validate-register&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7&quot;]/div[@class=&quot;form-group&quot;]/select[@class=&quot;select2 form-control&quot;]</value>
      <webElementGuid>9b2f43b5-3d2d-4ce2-9349-32abddbd2abe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='tujuan-penggunaan']</value>
      <webElementGuid>45c6a34e-9cb3-4180-ae1a-e420fe6f235b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate-register']/div/div[2]/div[8]/select</value>
      <webElementGuid>da4d39a8-75f9-4b10-8227-c0a971a45e1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[8]/following::select[1]</value>
      <webElementGuid>f0a54679-99c2-4431-a556-1920b7e91098</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saya telah membaca dan menyetujui ketentuan Pendaftaran Akun Data Online'])[1]/preceding::select[1]</value>
      <webElementGuid>a3d9bf91-b696-420c-9c45-c0094123a408</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/select</value>
      <webElementGuid>8be3d1e7-e4df-4b9e-9162-4665f97153bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'tujuan-penggunaan' and (text() = '
						 --Pilih--
																					
																			Kegiatan operasional dan pengembangan produk																	
															
																			Layanan meteorologi																	
															
																			Penelitian tentang data meteorologi																	
											    						' or . = '
						 --Pilih--
																					
																			Kegiatan operasional dan pengembangan produk																	
															
																			Layanan meteorologi																	
															
																			Penelitian tentang data meteorologi																	
											    						')]</value>
      <webElementGuid>012b77e1-c2a8-4354-a9f7-6fd6eab87fce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
