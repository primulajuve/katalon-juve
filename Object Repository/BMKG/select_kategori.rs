<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_kategori</name>
   <tag></tag>
   <elementGuidId>cc763eeb-5903-4527-bc7b-1427dc1c9408</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;kategori&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='kategori']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>e5f5c177-992b-4198-93d9-300279032e25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>kategori</value>
      <webElementGuid>11b680b9-d6f6-4ac4-81f8-e20440f38f29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2 form-control</value>
      <webElementGuid>db0a3320-d36a-42a4-a2fd-95c85bc2e282</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						 --Pilih--
																					
																			BUMN																	
															
																			Dosen																	
															
																			Instansi pemerintah																	
															
																			Mahasiswa																	
															
																			Peneliti																	
															
																			Swasta																	
											    						</value>
      <webElementGuid>1aa51c27-ba18-49ad-8153-24592f766330</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-validate-register&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7&quot;]/div[@class=&quot;form-group&quot;]/select[@class=&quot;select2 form-control&quot;]</value>
      <webElementGuid>8a2df352-8e3f-4fd7-87b3-f7861faba8b9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='kategori']</value>
      <webElementGuid>2dfd7a33-0d8f-4d6f-b890-5f1609894af8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate-register']/div/div[2]/div[6]/select</value>
      <webElementGuid>8fc6bc64-69d0-476b-a9bd-9d60a949ea19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[6]/following::select[1]</value>
      <webElementGuid>16c47c74-dfcd-4364-a094-dadf82f81730</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[7]/preceding::select[1]</value>
      <webElementGuid>931852ef-1ddb-41ee-a263-a265b68ed60c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/select</value>
      <webElementGuid>e714d01d-c344-43fd-b2e7-ed5476d9f000</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'kategori' and (text() = '
						 --Pilih--
																					
																			BUMN																	
															
																			Dosen																	
															
																			Instansi pemerintah																	
															
																			Mahasiswa																	
															
																			Peneliti																	
															
																			Swasta																	
											    						' or . = '
						 --Pilih--
																					
																			BUMN																	
															
																			Dosen																	
															
																			Instansi pemerintah																	
															
																			Mahasiswa																	
															
																			Peneliti																	
															
																			Swasta																	
											    						')]</value>
      <webElementGuid>c220782f-914e-40e5-9afd-12ef6b5fc6c6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
