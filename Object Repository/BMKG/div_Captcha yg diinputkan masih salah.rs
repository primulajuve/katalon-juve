<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Captcha yg diinputkan masih salah</name>
   <tag></tag>
   <elementGuidId>3af7f338-0af1-4d5f-8ee7-c7c8533d502d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.notify-message.alert-danger</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>62162ab3-cbb5-4488-908a-83c84ddda127</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert notify-message alert-danger</value>
      <webElementGuid>462c4f22-71a1-48ef-9f78-aeb013a30d4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>×Captcha yg diinputkan masih salah</value>
      <webElementGuid>3cd10257-5ba0-40df-8741-e3e1e0ba44bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;notify notify-top-center&quot;]/div[@class=&quot;alert notify-message alert-danger&quot;]</value>
      <webElementGuid>0fabe2e0-f5c9-44d8-bfc7-687c87e2d09a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[3]</value>
      <webElementGuid>dbf48725-0ff4-4294-8d1f-c7219724202f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::div[3]</value>
      <webElementGuid>5e8a0445-e8d4-4446-8896-e50f706dac9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div</value>
      <webElementGuid>79ab5542-3674-4410-8a2f-fe52067b75e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '×Captcha yg diinputkan masih salah' or . = '×Captcha yg diinputkan masih salah')]</value>
      <webElementGuid>2939aa97-b882-4406-a30b-60cee7f81bf4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
