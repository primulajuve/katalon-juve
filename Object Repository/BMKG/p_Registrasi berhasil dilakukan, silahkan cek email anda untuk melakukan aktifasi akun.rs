<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Registrasi berhasil dilakukan, silahkan cek email anda untuk melakukan aktifasi akun</name>
   <tag></tag>
   <elementGuidId>f03b29e2-ddbe-45f4-9c43-7e916a4542c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Selamat !'])[1]/following::p[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>f025ad3f-6f00-46bc-a06f-5f4b85e842c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>align</name>
      <type>Main</type>
      <value>center</value>
      <webElementGuid>e5605fe0-2387-4727-8193-85116c2fceb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Registrasi berhasil dilakukan, silahkan cek email anda untuk melakukan aktifasi akun</value>
      <webElementGuid>383b4c61-df7a-4573-82d2-d58fa69543b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;page-container&quot;]/div[@class=&quot;page-content&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;portlet light bordered&quot;]/div[@class=&quot;portlet-body&quot;]/div[@class=&quot;portlet light bordered&quot;]/div[@class=&quot;portlet-body&quot;]/p[1]</value>
      <webElementGuid>0d9cd11d-0063-4b0b-8772-8b34db82a291</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Selamat !'])[1]/following::p[1]</value>
      <webElementGuid>e97d8d2d-4c1d-4c9a-a941-68890b07b210</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/following::p[1]</value>
      <webElementGuid>1b789df7-5ea1-49dd-a328-a1a07b72c21a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tentang Aplikasi'])[1]/preceding::p[1]</value>
      <webElementGuid>d221daf5-4f4b-4c94-88c4-7357550fb342</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[1]/preceding::p[2]</value>
      <webElementGuid>4ba55227-fde1-4995-b06b-dee10b2dc2d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Registrasi berhasil dilakukan, silahkan cek email anda untuk melakukan aktifasi akun']/parent::*</value>
      <webElementGuid>d471a0d4-fecd-4fd2-92ad-4cec9045384b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>3a4c87c1-40d3-4c85-b928-044479b5ea23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Registrasi berhasil dilakukan, silahkan cek email anda untuk melakukan aktifasi akun' or . = 'Registrasi berhasil dilakukan, silahkan cek email anda untuk melakukan aktifasi akun')]</value>
      <webElementGuid>5b30e1eb-8c62-4b4c-9936-10acb46bb443</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
