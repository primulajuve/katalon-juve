<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Selamat</name>
   <tag></tag>
   <elementGuidId>9e2a64b8-7105-47ec-ba35-2b6bb171cc9d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/following::h1[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.portlet-body > h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>7c1ccdb2-6415-4c9e-898e-182cd792109f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>align</name>
      <type>Main</type>
      <value>center</value>
      <webElementGuid>2d1d66e2-cd2a-4558-8194-58883d70b6e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Selamat !</value>
      <webElementGuid>ff264d0e-b6d4-4fa4-88d7-3afd795217a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;page-container&quot;]/div[@class=&quot;page-content&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;portlet light bordered&quot;]/div[@class=&quot;portlet-body&quot;]/div[@class=&quot;portlet light bordered&quot;]/div[@class=&quot;portlet-body&quot;]/h1[1]</value>
      <webElementGuid>55cb45ce-465b-418e-be57-5c380ce40658</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/following::h1[1]</value>
      <webElementGuid>6a05fd73-ca84-43e4-aefb-24f150a2db72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::h1[1]</value>
      <webElementGuid>731a8097-2c0b-4485-8cdd-03ea300e62c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tentang Aplikasi'])[1]/preceding::h1[1]</value>
      <webElementGuid>49e704ff-3fe2-4c32-bc0c-f4926e09fd43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[1]/preceding::h1[1]</value>
      <webElementGuid>ae4805df-e27b-4b61-9e87-463599802563</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Selamat !']/parent::*</value>
      <webElementGuid>056ee557-fdab-47c0-bb00-bfffb2fbdd50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/h1</value>
      <webElementGuid>01f2bcf2-a8c0-4bb2-816b-7036399aad2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Selamat !' or . = 'Selamat !')]</value>
      <webElementGuid>155811ce-1029-40c4-bb4b-c64c7fe4effb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
