<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Pilih--BUMNDosenInstansi pemerinta_e459d1</name>
   <tag></tag>
   <elementGuidId>c1db35c5-323b-4d20-9336-75dfd0d502bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='kategori']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;kategori&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>bec539f9-1e6c-4027-ac7a-109a0e7b5988</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>kategori</value>
      <webElementGuid>0c528f65-72e4-4fa9-a771-82574c5f485d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2 form-control</value>
      <webElementGuid>6358b1aa-2d31-4474-91a5-34f0c4dcae72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						 --Pilih--
																					
																			BUMN																	
															
																			Dosen																	
															
																			Instansi pemerintah																	
															
																			Mahasiswa																	
															
																			Peneliti																	
															
																			Swasta																	
											    						</value>
      <webElementGuid>c51dcac3-b54b-4832-a02d-f87a944329fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-validate-register&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7&quot;]/div[@class=&quot;form-group&quot;]/select[@class=&quot;select2 form-control&quot;]</value>
      <webElementGuid>3f969b2e-7076-4bbc-9b38-04f8c6c8c0c6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='kategori']</value>
      <webElementGuid>e75045c9-bafb-4fcf-bc5f-89c09773302d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate-register']/div/div[2]/div[6]/select</value>
      <webElementGuid>e38eaf91-c864-46ff-932b-598c5041d70a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[6]/following::select[1]</value>
      <webElementGuid>4b51916c-b800-4bb0-970b-ec5fd85a0727</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[7]/preceding::select[1]</value>
      <webElementGuid>6285fdca-fa69-47aa-bf65-f44374163868</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/select</value>
      <webElementGuid>01737148-1eae-47db-a12a-9dbe6ea196c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'kategori' and (text() = '
						 --Pilih--
																					
																			BUMN																	
															
																			Dosen																	
															
																			Instansi pemerintah																	
															
																			Mahasiswa																	
															
																			Peneliti																	
															
																			Swasta																	
											    						' or . = '
						 --Pilih--
																					
																			BUMN																	
															
																			Dosen																	
															
																			Instansi pemerintah																	
															
																			Mahasiswa																	
															
																			Peneliti																	
															
																			Swasta																	
											    						')]</value>
      <webElementGuid>9adda5f2-fcd3-42a5-a720-744ed67ebfa5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
