<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Pilih--Kegiatan operasional dan pe_6c5da3</name>
   <tag></tag>
   <elementGuidId>09be1f2a-413e-4b7c-81b3-f42be6f842fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='tujuan-penggunaan']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;tujuan-penggunaan&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b8fea6eb-dad4-459f-aed0-ffa71070f98e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>tujuan-penggunaan</value>
      <webElementGuid>5744c603-8467-46f9-8aac-8f21a13d7985</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2 form-control</value>
      <webElementGuid>c0305729-de22-4bb6-a644-48f0510a7b1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						 --Pilih--
																					
																			Kegiatan operasional dan pengembangan produk																	
															
																			Layanan meteorologi																	
															
																			Penelitian tentang data meteorologi																	
											    						</value>
      <webElementGuid>9f135597-ef09-4edc-84c6-3e0bc78e25dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-validate-register&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7&quot;]/div[@class=&quot;form-group&quot;]/select[@class=&quot;select2 form-control&quot;]</value>
      <webElementGuid>cd6cffba-76ec-46f1-9723-1fd9bde00e1c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='tujuan-penggunaan']</value>
      <webElementGuid>919affe6-fda8-4502-b0b9-be466a4252d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate-register']/div/div[2]/div[8]/select</value>
      <webElementGuid>0548cd05-196c-4b5c-bea3-a0bccbf8e94d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[8]/following::select[1]</value>
      <webElementGuid>f8447a47-613a-4ea9-80f8-5f74d02672d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saya telah membaca dan menyetujui ketentuan Pendaftaran Akun Data Online'])[1]/preceding::select[1]</value>
      <webElementGuid>b7764097-5a87-42d1-acc4-9b93fc86d069</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/select</value>
      <webElementGuid>d819a1e3-5c7c-4020-8f60-688739ab7a74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'tujuan-penggunaan' and (text() = '
						 --Pilih--
																					
																			Kegiatan operasional dan pengembangan produk																	
															
																			Layanan meteorologi																	
															
																			Penelitian tentang data meteorologi																	
											    						' or . = '
						 --Pilih--
																					
																			Kegiatan operasional dan pengembangan produk																	
															
																			Layanan meteorologi																	
															
																			Penelitian tentang data meteorologi																	
											    						')]</value>
      <webElementGuid>78c7b0b2-3de3-4a93-92b3-c2b912d9fabf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
