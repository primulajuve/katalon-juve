import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dataonline.bmkg.go.id/register')

WebUI.setText(findTestObject('Object Repository/BMKG/input__email'), 'masyur mas')

WebUI.setText(findTestObject('Object Repository/BMKG/input__sandi'), '123456')

WebUI.setText(findTestObject('Object Repository/BMKG/input__konfirmasi-sandi'), '123456')

WebUI.setText(findTestObject('Object Repository/BMKG/input__nama-depan'), 'Dowe Anggoro')

WebUI.click(findTestObject('BMKG/span_negara'))

WebUI.setText(findTestObject('BMKG/input__cari_negara'), 'Indonesia')

WebUI.sendKeys(findTestObject('BMKG/input__cari_negara'), Keys.chord(Keys.ENTER))

WebUI.selectOptionByValue(findTestObject('BMKG/select_kategori'), '1', false)

WebUI.click(findTestObject('BMKG/span_instansi'))

WebUI.setText(findTestObject('BMKG/input__instansi'), 'Perusahaan Swasta')

WebUI.sendKeys(findTestObject('BMKG/input__instansi'), Keys.chord(Keys.ENTER))

WebUI.selectOptionByValue(findTestObject('Object Repository/BMKG/select_--Pilih--Kegiatan operasional dan pe_6c5da3'), '2', 
    true)

WebUI.click(findTestObject('Object Repository/BMKG/input__agreement'))

WebUI.click(findTestObject('Object Repository/BMKG/button__reload'))

WebUI.delay(6)

WebUI.click(findTestObject('Object Repository/BMKG/button_Daftar'))

WebUI.verifyElementPresent(findTestObject('BMKG/span_Bagian email harus berisi email yang sah'), 3)

