import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dataonline.bmkg.go.id/register')

WebUI.setText(findTestObject('BMKG/input__email'), 'data123@oke.com')

WebUI.setText(findTestObject('BMKG/input__sandi'), '12345')

WebUI.setText(findTestObject('BMKG/input__konfirmasi-sandi'), '12345')

WebUI.setText(findTestObject('BMKG/input__nama-depan'), 'Sihab Adha')

WebUI.click(findTestObject('BMKG/span_negara'))

WebUI.setText(findTestObject('BMKG/input__cari_negara'), 'Indonesia')

WebUI.sendKeys(findTestObject('BMKG/input__cari_negara'), Keys.chord(Keys.ENTER))

WebUI.selectOptionByValue(findTestObject('BMKG/select_kategori'), '1', false)

WebUI.click(findTestObject('BMKG/span_instansi'))

WebUI.setText(findTestObject('BMKG/input__instansi'), 'Perusahaan Swasta')

WebUI.sendKeys(findTestObject('BMKG/input__instansi'), Keys.chord(Keys.ENTER))

WebUI.selectOptionByValue(findTestObject('BMKG/select_tujuan'), '3', true)

WebUI.click(findTestObject('BMKG/input__agreement'))

WebUI.click(findTestObject('BMKG/i__fa fa-refresh'))

WebUI.delay(8, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('BMKG/span_Daftar'))

WebUI.verifyElementPresent(findTestObject('BMKG/h1_Selamat'), 3)

